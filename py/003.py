#!/usr/bin/env python3

from euler import isPrime

num = 600851475143
root = int(num**0.5) + 1
if root % 2 == 0:
    root -= 1

smaller = 3
larger = root
while smaller < root:
    if num % smaller == 0 and isPrime(num / smaller):
        print(smaller)
        break
    if num % larger == 0 and isPrime(larger):
        print(larger)
        break
    smaller += 2
    larger -= 2
