#!/usr/bin/env python3

import math

Limit = int(1e6)
TargetFamilySize = 8

PrimeSieve: list[bool]


def create_sieve(limit):
    global PrimeSieve
    PrimeSieve = [True] * Limit
    PrimeSieve[0] = False
    PrimeSieve[1] = False
    for i in range(2, limit):
        for j in range(2 * i, limit, i):
            PrimeSieve[j] = False


def is_prime(n):
    global PrimeSieve
    return PrimeSieve[n]


def replace_digit(n, pos, digit):
    radix = 10 ** pos
    radix_plus_one = 10 * radix
    lower_part = n % radix_plus_one
    lower_sans_digit = int(lower_part / radix) * radix
    return n - lower_sans_digit + digit * radix


def replace_digits(n, pattern, digit):
    for replace_position in pattern:
        n = replace_digit(n, replace_position, digit)
    return n


def pattern_is_same_digit(n, pattern):
    digits: set[int] = set()
    for pos in pattern:
        digit = int((n % (10 ** (pos + 1))) / (10 ** (pos)))
        digits.add(digit)
    return len(digits) == 1


def digit_replacement_patterns(n):
    digit_count = math.ceil(math.log10(n + 1))

    # Don't replace the bottom digit
    digit_count -= 1
    replace_possibilities = 2 ** digit_count
    for i in range(1, replace_possibilities):
        replacements: list[int] = list()
        for digit in range(digit_count):
            if i & (1 << digit):
                # Again, don't replace the bottom digit
                replacements.append(digit + 1)
        yield replacements


def prime_family_size_met(n):
    high_digit = int(math.log10(n + 1))
    for pattern in digit_replacement_patterns(n):
        # Not every prime is suitable.
        if not pattern_is_same_digit(n, pattern):
            continue

        # We can't replace the high digit with a zero.
        start_digit = 1 if high_digit in pattern else 0

        # Try each digit and see how many primes we get.
        prime_count = 0
        for digit in range(start_digit, 10):
            x = replace_digits(n, pattern, digit)
            prime_count += is_prime(x)

        if prime_count >= TargetFamilySize:
            return n

    return None


def main():
    global PrimeSieve

    create_sieve(Limit)

    # Only look at primes with at least 5 digits.
    primes = [p for p, prime in enumerate(PrimeSieve) if prime and math.log10(p + 1) >= 5]
    for p in primes:
        if prime_family_size_met(p):
            print(p)
            return


if __name__ == '__main__':
    main()
