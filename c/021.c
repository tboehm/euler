#include <stdio.h>
#include "Euler.h"

int main(void)
{
    int total = 0;
    int i;
    for (i = 1; i < 10000; i++) {
        if (isAmicable(i)) {
            total += i;
        }
    }
    printf("%d\n", total);
    return 0;
}

