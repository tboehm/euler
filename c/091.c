#include <stdio.h>

int isRightTriangle(int x1, int y1, int x2, int y2);

int main(void)
{
    int x1, y1, x2, y2;

    int count = 0;

    for (x1 = 0; x1 <= 50; x1++) {
        for (y1 = 0; y1 <= 50; y1++) {
            if (x1 == 0 && y1 == 0) { continue; }

            for (x2 = 0; x2 <= 50; x2++) {
                for (y2 = 0; y2 <= 50; y2++) {
                    if (x2 == 0 && y2 == 0) { continue; }
                    if (x2 == x1 && y2 == y1) { continue; }

                    /* Notice: This method double-counts every triangle.
                     * It would probably be better to generate triangles
                     * that I know are right-angled rather than test
                     * every triangle I generate.
                     */
                    if (isRightTriangle(x1, y1, x2, y2)) {
                        count++;
                    }
                }
            }
        }
    }

    printf("%d\n", count/2);
    return 0;
}

int isRightTriangle(int x1, int y1, int x2, int y2)
{
    int s1 = x1*x1+y1*y1;
    int s2 = x2*x2+y2*y2;
    int s3 = (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);

    return (s1 == s2 + s3) || (s2 == s1 + s3) || (s3 == s1 + s2);
}
