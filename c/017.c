#include <stdio.h>
#include <string.h>

char* ones[] = {"", "one", "two", "three", "four",
    "five", "six", "seven", "eight", "nine"};
char* teens[] = {"ten", "eleven", "twelve", "thirteen", "fourteen",
    "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
char* tens[] = {"", "", "twenty", "thirty", "forty",
    "fifty", "sixty", "seventy", "eighty", "ninety"};
char* hundred = "hundred";
char* and = "and";

int countLetters(int num);

int main(void)
{
    int i;
    int total = strlen("onethousand"); /* Just handle 1000 here */
    printf("%d: %d letters\n", 999, countLetters(999));
    for (i = 1; i < 1000; i++) {
        //printf("%d: %d letters\n", i, countLetters(i));
        total += countLetters(i);
    }
    printf("%d\n", total);
    return 0;
}

int countLetters(int num)
{
    int ones_digit = num % 10;
    int tens_digit = (num / 10) % 10;
    int hundreds_digit = (num / 100) % 10;
    int count = 0;
    if (tens_digit == 1) {
        count += strlen(teens[ones_digit]);
    } else {
        count += strlen(ones[ones_digit]);
        count += strlen(tens[tens_digit]);
    }
    if (hundreds_digit != 0) {
        if (count != 0) {
            count += strlen(and);
        }
        count += strlen(hundred) + strlen(ones[hundreds_digit]);
    }
    return count;
}

