import Data.List -- elemIndex
import Unique -- unique

rightPair :: (Integral a) => (a, a, a) -> Bool
rightPair (a, b, c) = a^2 + b^2 == c^2

pairSum :: (Integral a) => (a, a, a) -> a
pairSum (a, b, c) = a + b + c

maxLength = 500
pairs = filter rightPair [(a, b, c)
                          | c <- [1..maxLength],
                            b <- [1..c],
                            a <- [1..b]]
sums = filter (< 1000) $ map pairSum pairs
(pairsums, counts) = unzip [(n, length $ filter (== n) sums)
                            | n <- unique sums]

main :: IO ()
main = case elemIndex (maximum counts) counts of
         Just n -> do print (pairsums !! n )
         Nothing -> print "Error: No pairs"
