module Unique where

import Data.List -- sort

unique :: Ord a => [a] -> [a]
unique x = unique_sorted $ sort x

unique_sorted :: Ord a => [a] -> [a]
unique_sorted [x] = [x]
unique_sorted [x, y]
  | x == y = [y]
  | otherwise = [x, y]
unique_sorted (x:y:xs)
  | x == y = unique_sorted $ [y] ++ xs
  | otherwise = [x] ++ (unique_sorted $ [y] ++ xs)
