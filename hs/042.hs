import Euler -- triangluar
import Strings -- removeChars
import Data.Char -- ord, chr
import Data.List.Split -- splitOn

triangles = map triangular [1..]

letterScore :: Char -> Int
letterScore c = ord c - ord 'A' + 1

wordScore :: String -> Int
wordScore s = sum $ map letterScore s

isTriangular :: Int -> Bool
isTriangular n = n `elem` (takeWhile (<= n) triangles)

main :: IO ()
main = do
    contents <- readFile "../common/p042_words.txt"
    let words = map (removeChars "\"") (splitOn "," contents)
    let scores = map wordScore words
    let high_score = length $ filter isTriangular scores
    print high_score
