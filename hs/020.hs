import Euler -- factorial, listify

solution = sum $ listify $ factorial 100

main :: IO ()
main = do print solution
