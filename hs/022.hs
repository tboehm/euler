import Data.Char
import Data.List
import Data.List.Split

scorename :: String -> Int
scorename [] = 0
scorename (c:cs) = ord c - ord 'A' + 1 + scorename cs

score :: String -> Int -> Int
score name index = scorename name * index

allscores :: [(String, Int)] -> Int
allscores [] = 0
allscores ((name, index):xs) = score name index + allscores xs

main :: IO ()
main = do
    contents <- readFile "../common/p022_names.txt"
    let quoted_names = splitOn "," contents
    let names = sort [[c | c <- name, not (c `elem` "\"")] | name <- quoted_names]
    print $ allscores $ zip names [1..]
