module Euler where

import Data.List -- sort

-- Convert a number n from base b1 to b2
--baseConv :: Integral a => a -> a -> a -> a
--baseConv n b1 b2
--  | 

-- 1234 % 2 == 0
-- 617 % 2 == 1
-- 308 % 2 == 0
-- 154 % 2 == 0
-- 77 % 2 == 1
binary :: Integral a => a -> a
binary n
  | n <= 1 = n
  | otherwise = (n `mod` 2) + 10 * binary (n `div` 2)

decimal :: Integral a => a -> a
decimal n
  | n <= 1 = n
  | otherwise = (n `mod` 10) + 2 * decimal (n `div` 10)

-- Number of Collatz steps to take n to 1
collatz :: Integral a => a -> a
collatz n
    | n == 1 = 1
    | even n = 1 + collatz(div n 2)
    |  odd n = 1 + collatz(3*n+1)

concatnums :: Integral a => a -> a -> [a]
concatnums a b = concat [listify a, listify b]

-- An (unsorted) list of a number's divisors. First grab all the pairs
-- by looking at x and n/x up to the square root of n. Then look at the
-- square root itself and add that on if it is a factor (if the number
-- is a actually a square). This avoids double-counting factors.
divisors :: Integral a => a -> [a]
divisors n = concat [ [x, n `div` x]
                      | x <- [1..rupSqrt(n)-1], mod n x == 0]
                      ++ [rupSqrt n | isSquare n]

-- A list of divisor pairs for a number. For example:
-- divPairs 36
--      [(1,36),(2,18),(3,12),(4,9),(6,6)]
divPairs :: Integral a => a -> [(a,a)]
divPairs n = [(x, n `div` x) | x <- [1 .. rupSqrt(n)], n `mod` x == 0]

-- Basic recursive definition of factorial
factorial :: Integral a => a -> a
factorial 0 = 1
factorial n = n * factorial (n-1)

-- Break an integer up into its digits in a list
-- listify 123
--      [1, 2, 3]
listify :: Integral a => a -> [a]
listify n
    | n < 10 = [n]
    | n >= 10 = listify (div n 10) ++ [mod n 10]

-- Join a list of digits into an integer
-- ulistify [1, 2, 3]
--      123
unlistify :: Integral a => [a] -> a
unlistify [x] = x
unlistify xs = last xs + 10 * (unlistify $ init xs)

-- First in a tuple of 3
first :: (Show a, Show b, Show c) => (a, b, c) -> a
first (x, _, _) = x

-- Second in a tuple of 3
second :: (Show a, Show b, Show c) => (a, b, c) -> b
second (_, x, _) = x

-- Third in a tuple of 3
third :: (Show a, Show b, Show c) => (a, b, c) -> c
third (_, _, x) = x

-- Combinations, n `choose` k
choose :: Integral a => a -> a-> a
choose n k = factorial n `div` (factorial k * factorial (n-k))

-- Check if a number is a circular prime (rotations of digits are prime)
isCircularPrime :: Integral a => a -> Bool
isCircularPrime n = and (map isPrime (lrotations n))

-- If a list of digits is the same as its reverse, it's a palindrome
isPal :: Integral a => a -> Bool
isPal n = listify n == reverse (listify n)

-- Check if a string is 1-9 pandigital
isPandigital :: String -> Bool
isPandigital x = (sort x) == "123456789"

-- Check if an integer is 1-9 pandigital
isPan :: (Integral a, Show a) => a -> Bool
isPan x = isPandigital $ show x

-- Return True if a number is prime, otherwise false
isPrime :: Integral a => a -> Bool
isPrime 0 = False
isPrime 1 = False
isPrime 2 = True
isPrime n
    | sum [x | x <- [2..rupSqrt $ abs n], mod n x == 0] == 0 = True
    | otherwise = False

-- Test whether a number is a square
isSquare :: Integral a => a -> Bool
isSquare n = (rupSqrt n)^2 == n

-- Rotate digits of a number n t times to the left. For example:
-- rotl 1 123
--        231
-- rotl 2 123
--        312
rotl :: Integral a => Int -> a -> a
rotl t n = unlistify $ (drop count digits) ++ (take count digits)
    where digits = listify n
          count = mod t (length digits)

-- Rotate digits of a number n t times once to the right. For example:
-- rotr 1 123
--        312
-- rotr 2 123
--        231
rotr :: Integral a => Int -> a -> a
rotr t n = unlistify $ (drop count digits) ++ (take count digits)
    where digits = listify n
          count = (length digits) - mod t (length digits)

-- Rotations (to the left) of a number. For example:
-- lrotations 123
--      [123, 231, 312]
lrotations :: Integral a => a -> [a]
lrotations n = zipWith (rotl) [0 .. num_digits - 1] [n, n ..]
    where num_digits = length $ listify n

-- Rotations (to the right) of a number. For example:
-- rrotations 123
--      [123, 312, 231]
rrotations :: Integral a => a -> [a]
rrotations n = zipWith (rotr) [0 .. num_digits - 1] [n, n ..]
    where num_digits = length $ listify n

-- Round-down square root of an int
rdnSqrt :: Integral a => a -> a
rdnSqrt n = floor $ sqrt $ fromIntegral n

-- Round-up square root of an int
rupSqrt :: Integral a => a -> a
rupSqrt n = ceiling $ sqrt $ fromIntegral n

-- The nth triangular number
triangular :: Integral a => a -> a
triangular n = div (n*(n+1)) 2
