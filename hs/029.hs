import Data.List  -- nub, sort

solution = length $ nub $ sort [a ** b | a <- [2 .. 100], b <- [2 .. 100]]

main :: IO ()
main = do print solution
