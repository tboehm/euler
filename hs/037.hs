import Euler -- isPrime, listify, unlistify

ltruncations :: Integral a => a -> [a]
ltruncations a = [unlistify (take n l) | n <- [1 .. len]]
    where l = listify a
          len = length l

rtruncations :: Integral a => a -> [a]
rtruncations a = [unlistify (drop n l) | n <- [len - 1, len - 2 .. 0]]
    where l = listify a
          len = length l

truncPrime :: Integral a => a -> Bool
truncPrime n = and $ map isPrime (rtruncations n ++ ltruncations n)

-- Single digit numbers don't count
solution = sum $ take 11 [n | n <- [10..], truncPrime n]

main :: IO ()
main = do print solution
