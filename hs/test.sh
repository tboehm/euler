#!/usr/bin/env sh


numeric_re='^[0-9]+$'

for f in `ls *.hs`; do
    base=`printf $f | cut -d. -f1`
    if [[ $base =~ $numeric_re ]]; then
        out=$base.bin
        make $out 2>&1 >/dev/null
        printf "$base ==> "
        ./$out
    fi
done
