slice :: (Show a) => [a] -> Int -> Int -> [a]
slice xs from to = take (to - from) (drop from xs)

slicelen :: (Show a) => [a] -> Int -> Int -> [a]
slicelen xs from len = take len (drop from xs)

permutations :: (Show a) => [a] -> [[a]]
permutations (x:xs) = [xs]

perm1 :: (Show a) => [a] -> [[a]]
perm1 s = [slice s n (n+1) 
        ++ slice s 0 n
        ++ slice s (n+1) (length s)
        | n <- [0..3]]

solution = (permutations "aoeu") !! 0

main :: IO ()
main = undefined
