-- Sum the corners on a spiraling-out square (e.g. 9 + 7 + 5 + 3)
sumcorners :: (Integer, Integer) -> Integer
sumcorners (1, _) = 1  -- The center is a special case
sumcorners (x, y) = 4 * x - 6 * y

-- Odd numbers (squared) paired with the even number preceding 
oddsquares = [(x * x, x - 1) | x <- [1..], mod x 2 == 1]
-- Pairs for the entire 1001x1001 grid
gridsquares = takeWhile (\x -> fst x <= 1001 * 1001) oddsquares

solution = sum $ map sumcorners gridsquares

main :: IO ()
main = do print solution
