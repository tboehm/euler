merge :: Integral a => [a] -> [a] -> [a]
merge [x] [] = [x]
merge [w, x] [y] = [max (w + y) (x + y)]
merge (w:x:xs) (y:ys) = merge [w, x] [y] ++ merge ([x] ++ xs) ys

collapse :: Integral a => [[a]] -> a
collapse [[x]] = x
collapse (lower:upper:xs) = collapse ([merge lower upper] ++ xs)

main = do
    contents <- readFile "../c/p018_triangle.txt"
    let split_triangle = [words line | line <- lines contents]
    let triangle = [[read word::Int | word <- line] | line <- split_triangle]
    let result = collapse (reverse triangle)
    print result
