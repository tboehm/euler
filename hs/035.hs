import Euler -- isCircularPrime

solution = length $ filter isCircularPrime [1 .. 1000000]

main :: IO ()
main = do print solution
