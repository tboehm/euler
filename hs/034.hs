import Euler -- factorial, listify

factorialSum :: Integral a => a -> a
factorialSum n = sum $ map factorial (listify n)

-- No single digits, so start with the lowest possible sum (1! + 0!)
-- TODO: 100000 is an arbitrary upper bound....
solution = sum $ [n | n <- [10..100000], n == factorialSum n]

main :: IO ()
main = do print solution
