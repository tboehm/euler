tens :: Int -> Int
tens x = (floor $ fromIntegral (x `div` 10)) `mod` 10

hundreds :: Int -> Int
hundreds x = (floor $ fromIntegral (x `div` 100)) `mod` 100

countLetters :: Int -> Int
countLetters 0    = 0 -- (zero is not included -> use a base case)
countLetters 1    = 3 -- one
countLetters 2    = 3 -- two
countLetters 3    = 5 -- three
countLetters 4    = 4 -- four
countLetters 5    = 4 -- five
countLetters 6    = 3 -- six
countLetters 7    = 5 -- seven
countLetters 8    = 5 -- eight
countLetters 9    = 4 -- nine
countLetters 10   = 3 -- ten
countLetters 11   = 6 -- eleven
countLetters 12   = 6 -- twelve
countLetters 13   = 8 -- thirteen
countLetters 14   = 8 -- fourteen
countLetters 15   = 7 -- fifteen
countLetters 16   = 7 -- sixteen
countLetters 17   = 9 -- seventeen
countLetters 18   = 8 -- eighteen
countLetters 19   = 8 -- nineteen
countLetters 20   = 6 -- twenty
countLetters 30   = 6 -- thirty
countLetters 40   = 5 -- forty
countLetters 50   = 5 -- fifty
countLetters 60   = 5 -- sixty
countLetters 70   = 7 -- seventy
countLetters 80   = 6 -- eighty
countLetters 90   = 6 -- ninety
countLetters 1000 = 11 -- onethousand
countLetters x
  | x < 100 = countLetters ((tens x) * 10)
            + countLetters (x `mod` 10)
  | x `mod` 100 == 0 = countLetters (hundreds x)
                     + 7 -- "hundred"
                     + countLetters (x `mod` 100)
  | otherwise = countLetters (hundreds x)
              + 7 -- "hundred"
              + 3 -- "and"
              + countLetters (x `mod` 100)

solution = sum [countLetters x | x <- [1..1000]]

main :: IO ()
main = do print solution
