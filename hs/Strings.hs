module Strings where

punctuation = ",.?!:;\"\'"

removeChars :: String -> String -> String
removeChars chars string = filter (not . (`elem` chars)) string

removePunc :: String -> String
removePunc string = removeChars punctuation string
