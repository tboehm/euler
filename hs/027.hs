import Euler -- isPrime, first, second, third

polyeval :: Integer -> Integer -> Integer -> Integer
polyeval a b n = n * n + a * n + b

polyprimes :: Integer -> Integer -> Int
polyprimes a b = length (takeWhile isPrime [polyeval a b n | n <- [0..]])

allstreaks = [(a, b, polyprimes a b) | a <- [-1000 .. 1000], b <- [-1000 .. 1000]]
smallstreaks = [(a, b, polyprimes a b) | a <- [0 .. 1000], b <- [0 .. 1000]]

--maxstreak :: [(Integer, Integer, Integer)] -> (Integer, Integer, Integer)
maxstreak :: [(Integer, Integer, Int)] -> (Integer, Integer, Int)
maxstreak [x] = x
maxstreak (x:xs)
  | third x > third y = x
  | otherwise = y
    where y = maxstreak xs

solution = first x * second x
    where x = maxstreak allstreaks

main :: IO ()
main = do print solution
