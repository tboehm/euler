import Euler -- isPal, binary

solution = sum [n | n <- [1 .. 1000000], isPal n, isPal $ binary n]

main :: IO ()
main = do print solution
