import Euler

sumpow :: (Integral a) => a -> a -> a
sumpow n a = sum $ map (\x -> x ^ a) (listify n)

-- I have not proven this as an upper bound, but it works
solution = sum [x | x <- [10..200000], sumpow x 5 == x]

main :: IO ()
main = do print solution
