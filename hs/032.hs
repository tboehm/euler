import Euler -- listify
import Unique -- unique

catFactors :: (Integral a, Show a) => a -> [String]
catFactors n = [show n ++ show x ++ show y | (x, y) <- divPairs n]

hasPanFactors :: (Integral a, Show a) => a -> Bool
hasPanFactors n = or $ map isPandigital (map unique (catFactors n))

panFactors :: (Integral a, Show a) => a -> a
panFactors n
  | hasPanFactors n = n
  | otherwise = 0

-- Arbitrary upper bound that seems about right...
solution = sum [panFactors n | n <- [1 .. 10000]]

main :: IO ()
main = do print solution
