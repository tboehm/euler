import Euler -- isPan, listify unlistify

catProd :: Integral a => a -> a -> a
catProd n x = unlistify $ concat (map listify (zipWith (*) [1..x] (repeat n)))

-- Highest n could be 4 digits: need to concatenate at least two powers.
-- Highest x could be 1 digit: 1^10 ++ 2^10 ++ 3^10 exceeds 9 digits.
nRange = [1..9999]
xRange = [2..9]
solution = maximum $ filter isPan [catProd n x
  | n <- nRange, x <- xRange]

main :: IO ()
main = do print solution
