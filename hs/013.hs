main :: IO()
main = do
    contents <- readFile "013.txt"
    let numbers = [read x::Integer | x <- lines contents]
    putStrLn $ take 10 $ show $ sum numbers
