import Euler -- listify

solution = sum $ listify $ 2^1000

main :: IO ()
main = do print solution
